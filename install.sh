#!/usr/bin/env bash

if [[ ! -d "$HOME/.local/bin" ]]; then
    mkdir -p "$HOME/.local/bin"
fi

cd "$(dirname "$0")"

cp bin/todo.sh "$HOME/.local/bin/todo"
chmod 755 "$HOME/.local/bin/todo"
