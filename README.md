# `todo.sh` : A basic todo script

A basic todo script that manages items through the filesystem.

By default the todo data sits in `$HOME/.local/var/todo.sh/`
