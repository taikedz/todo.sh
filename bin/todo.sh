#!/usr/bin/env bash

set -euo pipefail

## Add new item:
##   todo.sh [-d DIRNAME] Title ...
##
## Show items:
##   todo.sh -l
##
## Show items, filtering on names (PCRE regex, case-insensitive)
##   todo.sh -f REGEX
##
## Search all MD file notes (PCRE regex, case-insensitive) (optionally specify additional grep flags):
##   todo.sh -s REGEX [GREPFLAGS ...]
##
## Move items:
##   todo.sh -m ID SECTION
##
## Default sections are "todo", "doing", done"
##
## Edit notes of an item (FILE by default is the 'README.md' file):
##   todo.sh -e ID [FILE]
##
## Add one line note with date to main notes:
##   todo.sh -n ID NOTES ...
##
## Mark completed, adding date:
##   todo.sh -c ID [NOTES ...]
##
## Archive into ZIP file:
##   todo.sh -a [ZIPPATH]
##
## ENV:
## TODO_SETUPGIT=true - set up a git repository in the item

TODO_data_location="$HOME/.local/var/todo.sh"

ensure_dir() {
    if [[ ! -d "$1" ]]; then
        mkdir -p "$1"
    fi
}

ensure_directories() {
    ensure_dir "$TODO_data_location/todo"
    ensure_dir "$TODO_data_location/doing"
    ensure_dir "$TODO_data_location/done"
    ensure_dir "$TODO_data_location/archive"
}

datenow() {
    date "+%F"
}

check_target_dir() {
    if [[ "$1" = -d ]]; then
        targetdir="$2"
        return 0
    fi
    return 1
}

get_task_id() {
    local taskfile="$TODO_data_location/.task_id"
    if [[ ! -f "$taskfile" ]]; then
        echo -n "1" > "$taskfile"
    fi

    tasknum="$(cat "$taskfile")"
    taskid="$(zfill "$tasknum")"

    echo "$((tasknum + 1))" > "$taskfile"
}

zfill() {
    local tasknum="$1"
    python -c "print('$tasknum'.zfill(4))"
}

mark_completed() {
    local id="$1"; shift
    local tasknum="$(zfill "$id")"; shift

    add_note "$id" "COMPLETED --" "$*"
    mvitem "$tasknum" done
}

add_note() {
    local tasknum="$(zfill "$1")"; shift
    local thedir="$(get_item_by_id "$tasknum")"

    if [[ -z "$*" ]]; then
        echo "No notes supplied!"
        exit 1
    fi

    echo -e "\n$(datenow): $*" | tee -a "$thedir/README.md"
}

get_item_by_id() {
    local id="${1:-}"; shift
    find . -maxdepth 2 -mindepth 2 -name "${id}--*" -type d
}

edit_item() {
    local tasknum="$(zfill "$1")"; shift
    local thedir="$(get_item_by_id "$tasknum")"
    /usr/bin/env "${EDITOR:-vim}" "$thedir/${1:-README.md}"
}

mvitem() {
    local tasknum="$(zfill "$1")"; shift
    local thedir="$(get_item_by_id "$tasknum")"
    local dest="${1:-}"; shift

    [[ -d "$dest/" ]] || {
        echo "'$dest' does not exist"
        exit 1
    }

    #FIXME check single item
    mv "$thedir" "$dest"
    echo "Moved '$thedir' to '$dest'"
}

show_section() {
    echo "---- $1"
    find "$2" "${FIND_TERMS[@]}"
    echo ""
}

show_items() {
    if [[ "${1:-}" =~ ^[0-9]+$ ]]; then
        local tasknum="$(zfill "$1")"; shift
        local thedir="$(get_item_by_id "$tasknum")"

        local targetfile="$thedir/${1:-README.md}"
        echo -e "$targetfile\n-----"
        /usr/bin/env tail -n 5 "$targetfile" | sed -r 's/^/    /'
        return
    fi

    FIND_TERMS=(-maxdepth 1 -type d -name '????--*')

    show_section Current doing
    show_section Queued todo
    show_section Finished done

    FIND_TERMS=(-maxdepth 1 -type f -name '*.zip')
    show_section Archived archive
}

filter_items_grep() {
    show_items | grep -iP "$1"
}

search_in_notes() {
    local regex="$1"; shift
    find . -type f -name '*.md' -exec grep -iPH "$@" "$regex" {} \;
}

check_for_args() {
    if [[ "$*" =~ --help ]]; then
        grep -P '^##' "$0"
        exit 0
    elif [[ -z "$*" ]]; then
        echo "No command specified. Try --help ?"
        exit 1
    fi
}

archive_item() {
    local zip_path"${1:-}"; shift || :
    local archive_name="$(datenow)-archive.zip"
    local canonical_zip="$TODO_data_location/archive/$archive_name"

    archive_create_zip "$canonical_zip"

    if [[ -n "${zip_path:-}" ]]; then
        cp "$canonical_zip" "$zip_path"
        echo "Archive copied to <$zip_path>"
    fi
}

archive_create_zip() {
    python -m zipfile -c "$1" "$TODO_data_location/done"/*
    echo "Archive created in <$1>"
    rm -r "$TODO_data_location/done"/*
    echo "'done' section cleared."
}

create_project() {
    targetdir="todo"

    if check_target_dir "$@"; then
        shift; shift
    fi

    get_task_id

    description="$*"
    filename="$(echo "$description" | sed -r 's/[^a-zA-Z0-9_.-]+/_/g')"
    project_dir="$targetdir/$taskid--$filename"
    mkdir "$project_dir"

    cat << EOTXT >> "$project_dir/README.md"
---
Date: $(datenow)
Author: $(whoami)
ID: $taskid
---

# $description

EOTXT

    if [[ "${TODO_SETUPGIT:-}" = true ]]; then
        (
        cd "$project_dir"
        git init .
        #git status
        )
    fi

    echo "Created '$project_dir'"
}

check_action() {
    case "$1" in
        -m)
            shift
            mvitem "$@"
            ;;
        -e)
            shift
            edit_item "$@"
            ;;
        -c)
            shift
            mark_completed "$@"
            ;;
        -n)
            shift
            add_note "$@"
            ;;
        -f)
            shift
            filter_items_grep "$@"
            ;;
        -s)
            shift
            search_in_notes "$@"
            ;;
        -l)
            shift
            show_items "$@"
            ;;
        -a)
            shift
            archive_item "$@"
            ;;
        -d)
            create_project "$@"
            ;;
        -*)
            # Any unrecognized option
            echo "Unkown option '$1'"
            exit 10
            ;;
        *)
            # Indeed, same as -d
            create_project "$@"
            ;;
    esac
}

main() {
    ensure_directories

    check_for_args "$@"

    cd "$TODO_data_location"

    check_action "$@"

}

main "$@"
